<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">   
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>                
                <title>Nuttapon Buajamrattanawong's MSN Log</title>
                <style type="text/css">
                    body
                    {
                    font-family: Verdana, arial, sans-serif;
                    }                  
                </style>      
            </head>          
            <body>                        
                <xsl:for-each select="//Message[1]">
                    <span style="color:red">[Conversation started on 
                        <xsl:value-of select="@Date"/> 
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="@Time"/>] 
                    </span>
                </xsl:for-each>                                                         
                <table border="0">                     
                    <xsl:for-each select="Log/Message">
                        <tr>
                            <td>                                                             
                                <xsl:text>[ </xsl:text>
                                <xsl:value-of select="@Date"/>     
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="@Time"/>                  
                                <xsl:text>]</xsl:text>                                                                    
                            </td>                            
                            <td>
                                <xsl:choose>
                                    <xsl:when test ="(position() mod 2 = 1)">
                                        <span style="color: orange">
                                            <xsl:value-of select="From/User/@FriendlyName"/>                                                                                                                                   
                                        </span>
                                    </xsl:when>                                  
                                    <xsl:otherwise>
                                        <span style="color: green">
                                            <xsl:value-of select="From/User/@FriendlyName"/>                                                                                                                                   
                                        </span>
                                    </xsl:otherwise>                                    
                                </xsl:choose>
                            </td>                                                                                   
                            <td>
                                <xsl:choose>
                                    <xsl:when test="(position() = (1)) or (position() = (3))">
                                        <span style="color: orange">
                                            <xsl:text>:</xsl:text>
                                            <xsl:value-of select="Text"/>                                
                                        </span>
                                    </xsl:when>                                    
                                    <xsl:otherwise>
                                        <span style="color: green">
                                            <xsl:text>:</xsl:text>
                                            <xsl:value-of select="Text"/>                                
                                        </span>
                                    </xsl:otherwise>                             
                                </xsl:choose>                                
                            </td>                            
                        </tr>
                    </xsl:for-each>                    
                </table>                                                                                                                                                                         
            </body>
        </html> 
    </xsl:template>   
</xsl:stylesheet>
