<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:lib="http://www.zvon.org/library" version="1.0">
    <xsl:output method="html" encoding="UTF-8"/>
    <xsl:template match="/">
        <html>            
            <body>
                <table border="1">
                    <tr>
                        <th>Title</th>
                        <th>Pages</th>
                    </tr>
                    <xsl:for-each select="//rdf:Description[position()>=3]">
                        <xsl:sort select="lib:pages"/>
                        <tr>
                            <td>
                                <xsl:value-of select="@about"/>
                            </td>
                            <td>
                                <xsl:number value="lib:pages" grouping-size="3"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
