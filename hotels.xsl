<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:kml="http://www.opengis.net/kml/2.2" version="1.0">
    <xsl:output method="html" encoding="UTF-8"/>
    <xsl:template match="/">
        <html>                   
            <h1> 
                <img>
                    <xsl:attribute name="src">
                        <xsl:value-of select="//kml:Icon/kml:href"/> 
                    </xsl:attribute>
                </img> 
                <xsl:value-of select="//kml:Document/kml:name"/>                 
            </h1>        
            <p>
                <xsl:text>List of hotels</xsl:text>
            </p>
            <xsl:for-each select="//kml:Placemark">
                <xsl:sort select="kml:name"/>
                <ul>
                    <li> 
                        <xsl:value-of select="kml:name"/>
                        <ul>
                            <li> 
                                <a>
                                    <xsl:attribute name="href">                                         
                                        <xsl:value-of select="substring-before(substring-after(kml:description,'href=&#34;'),'&#34;&gt;&lt;img')"/>                                         
                                    </xsl:attribute>                                    
                                    <xsl:value-of select="substring-before(substring-after(kml:description,'href=&#34;'),'&#34;&gt;&lt;img')"/>                                 
                                </a>                                
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <xsl:text>Coordinates:</xsl:text> 
                                <xsl:value-of select="kml:Point/kml:coordinates"/> 
                            </li>
                        </ul>                        
                    </li>
                </ul>
            </xsl:for-each>
        </html>
    </xsl:template>
</xsl:stylesheet>
